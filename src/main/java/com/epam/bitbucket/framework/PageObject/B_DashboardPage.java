package com.epam.bitbucket.framework.PageObject;

import com.epam.bitbucket.framework.Parent;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindAll;
import org.openqa.selenium.support.FindBy;
import com.epam.bitbucket.framework.PageObject.Repositories.*;
import java.util.List;

import static com.epam.bitbucket.utility.Utility.readFromPropertiesFile;
import static com.epam.bitbucket.utility.Utility.waitUntilElementToBeClickable;
import static com.epam.bitbucket.utility.Utility.waitUntilVisibilityOfAllElements;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;

/**
 * Created by Anita_Horvath on 20/02/2017.
 */
public class B_DashboardPage extends Parent {

    @FindBy (className = "aui-header-logo-device")
    private WebElement logo;

    @FindBy(id = "user-dropdown-trigger")
    private WebElement userDropdown;

    @FindBy(className = "aid-profile--name")
    private WebElement profileName;

    @FindBy(id = "log-out-link")
    private WebElement logOutButton;

    @FindBy(className = "aid-profile--email")
    private WebElement profileEmail;

    @FindBy(id = "repositories-dropdown-trigger")
    private WebElement repoDropdown;

    @FindBy(id = "create-repo-link")
    private WebElement createRepoLink;

    @FindBy(css = "a[href=\"/dashboard/repositories\"]")
    private WebElement repoButton;

    @FindAll({@FindBy (css = "a[class$=\"repo-list--repo-name\"]")})
    private List<WebElement> repoList;

    @FindBy(css = "input[type=\"search\"][placeholder=\"Find repositories\"]")
    private WebElement findRepositoriesButton;

    public B_DashboardPage(WebDriver driver){
        super(driver);
    }

    public void pressLogo(){
        logo.click();
    }

    public void pressCreateRepoLink(){
        createRepoLink.click();
    }

    public void pressRepoDropdown(){
        repoDropdown.click();
        //repoDropdown.click();
    }

    public void pressUserDropdown(){
        waitUntilElementToBeClickable(userDropdown, getDriver());
        userDropdown.click();
    }

    public void pressLogOutButton(){
        logOutButton.click();
    }

    public String getProfileName(){
        waitUntilElementToBeClickable(profileName, getDriver());
        return profileName.getText();
    }

    public String getProfileEmail(){
        return profileEmail.getText();
    }

    public void pressRepoButton(){
        repoButton.click();
    }

    public List<WebElement> getRepoList(){
        waitUntilVisibilityOfAllElements(repoList,getDriver());
        return repoList;
    }

    public void validateLogIn(){
        userDropdown.click();
        assertThat("Validate username",profileName.getText(), is(readFromPropertiesFile("NAME")));
        assertThat("Validate user email",profileEmail.getText(), is(readFromPropertiesFile("EMAIL")));
    }

    public void createFirstRepo(String name, boolean privateRepo){
        repoButton.click();
        B_RepositoriesPage repositoriesPage = new B_RepositoriesPage(getDriver());
        repositoriesPage.createRepo(name, privateRepo);
        logo.click();
    }

    public void createRepo(String name, boolean privateRepo){
        repoDropdown.click();
        repoDropdown.click();
        createRepoLink.click();
        B_CreateRepositoriesPage createRepositoriesPage = new B_CreateRepositoriesPage(getDriver());
        createRepositoriesPage.createRepo(name, privateRepo);
        logo.click();
    }

    public void validateCreateRepo(String name, boolean privateRepo){
        boolean repoIsCreated = false;
        repoButton.click();
        waitUntilVisibilityOfAllElements(repoList,getDriver());
        for (WebElement e : repoList) {
            //System.out.println(e.getText());
            if (e.getText().equals(name)) {
                e.click();
                B_RepositoryPage repoPage = new B_RepositoryPage(getDriver());
                repoIsCreated = repoPage.validateRepoSettings(privateRepo);
                break;
            }
        }
        assertThat("Repo is created",repoIsCreated, is(true));
        logo.click();
    }

    public void deleteRepo(String name){
        repoButton.click();
        waitUntilVisibilityOfAllElements(repoList,getDriver());
        for (WebElement e : repoList) {
            if (e.getText().equals(name)) {
                e.click();
                B_RepositoryPage repoPage = new B_RepositoryPage(getDriver());
                repoPage.deleteRepo();
                break;
            }
        }
        logo.click();
    }

    public void logOut(){
        userDropdown.click();
        logOutButton.click();
    }
}
