package com.epam.bitbucket.framework.PageObject;

import com.epam.bitbucket.framework.Parent;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import static com.epam.bitbucket.utility.Utility.readFromPropertiesFile;

/**
 * Created by Anita_Horvath on 20/02/2017.
 */
public class B_HomePage extends Parent{

    @FindBy(css = "a[href$=\"bitbucket.org/account/signin/\"]")
    private WebElement loginButton;

    public B_HomePage(WebDriver driver){
            super(driver);
        }

    public void loadSite(){
        getDriver().get(readFromPropertiesFile("BITBUCKET_URL"));
    }

    public void pressLogInButton(){
        loginButton.click();
    }

    public void logIn(){
        loginButton.click();
        B_LogInPage logIn = new B_LogInPage(getDriver());
        logIn.logIn();
    }

    public void signUp(){
        loginButton.click();
        B_LogInPage logIn = new B_LogInPage(getDriver());
        logIn.signUp();
    }

}
