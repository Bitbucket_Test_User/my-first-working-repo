package com.epam.bitbucket.framework.PageObject;

import com.epam.bitbucket.framework.PageObject.CreateAccount.B_CreateAccountPage;
import com.epam.bitbucket.framework.Parent;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import static com.epam.bitbucket.utility.Utility.readFromPropertiesFile;
import static com.epam.bitbucket.utility.Utility.waitUntilElementToBeClickable;

/**
 * Created by Anita_Horvath on 20/02/2017.
 */
public class B_LogInPage extends Parent{

    @FindBy(id="signup")
    private WebElement signupButton;

    @FindBy(id = "js-email-field")
    private WebElement emailField;

    @FindBy(id = "js-password-field")
    private WebElement passwordField;

    @FindBy(css = "input[type=\"submit\"][class$=\"aui-button-primary\"]")
    private WebElement logInButton;

    public B_LogInPage(WebDriver driver){
        super(driver);
    }

    public void pressSignUpButton(){
        signupButton.click();
    }

    public void fillEmailField(String email){
        waitUntilElementToBeClickable(emailField, getDriver());
        emailField.sendKeys(email);
    }

    public void fillPasswordField(String pwd){
        passwordField.sendKeys(pwd);
    }

    public void pressLogInButton(){
        logInButton.click();
    }

    public void logIn(){
        emailField.sendKeys(readFromPropertiesFile("EMAIL"));
        passwordField.sendKeys(readFromPropertiesFile("PASSWORD"));
        passwordField.submit();
    }

    public void signUp(){
        signupButton.click();
        B_CreateAccountPage createAccount = new B_CreateAccountPage(getDriver());
        createAccount.signUp();
    }
}
