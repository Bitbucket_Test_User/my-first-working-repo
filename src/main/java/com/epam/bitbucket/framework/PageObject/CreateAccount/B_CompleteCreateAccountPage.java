package com.epam.bitbucket.framework.PageObject.CreateAccount;

import com.epam.bitbucket.framework.Parent;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import static com.epam.bitbucket.utility.Utility.waitUntilElementToBeClickable;

/**
 * Created by Anita_Horvath on 20/02/2017.
 */
public class B_CompleteCreateAccountPage extends Parent{

    @FindBy(id="js-full-name-field")
    private WebElement nameField;

    @FindBy(id="js-password-field")
    private WebElement passwordField;

    @FindBy(className = "recaptcha-checkbox-checkmark")
    //@FindBy(xpath = ".//*[@id='recaptcha-anchor']/div[5]")
    private WebElement captcha;

    @FindBy(id="js-continue-cta-link")
    private WebElement continueButton;

    @FindBy(name = "undefined")
    private WebElement captchaFrame;

    public B_CompleteCreateAccountPage(WebDriver driver){
        super(driver);
    }

    public void fillNameField(String name){
        waitUntilElementToBeClickable(nameField, getDriver());
        nameField.sendKeys(name);
    }

    public void fillPasswordField(String pwd){
        passwordField.sendKeys(pwd);
    }

    public void checkCaptcha(){
        getDriver().switchTo().frame(captchaFrame);
        captcha.click();
        // TODO select the right pictures on the Captcha
        getDriver().switchTo().defaultContent();
    }

    public void pressContinueButton(){
        continueButton.click();
    }

    public void completeSignUp(){
        // TODO
        waitUntilElementToBeClickable(nameField, getDriver());
        nameField.sendKeys("Auto Test User");
        passwordField.sendKeys("Autotest123");

        getDriver().switchTo().frame(captchaFrame);
        captcha.click();
        // TODO select the right pictures on the Captcha

        getDriver().switchTo().defaultContent();
        continueButton.click();
        // TODO check your inbox and set username at almost done

    }
}
