package com.epam.bitbucket.framework.PageObject.CreateAccount;

import com.epam.bitbucket.framework.PageObject.Nincsmail.NincsmailPage;
import com.epam.bitbucket.framework.Parent;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

/**
 * Created by Anita_Horvath on 20/02/2017.
 */
public class B_CreateAccountPage extends Parent {

    @FindBy(id="js-email-proxy-field")
    private WebElement emailField;

    @FindBy(id="js-continue-cta-link")
    private WebElement continueButton;

    public B_CreateAccountPage(WebDriver driver){
        super(driver);
    }

    public void fillEmailField(String email){
        emailField.sendKeys(email);
    }

    public void pressContinueButton(){
        continueButton.click();
    }

    public void signUp(){
        // TODO
        NincsmailPage nincsmailPage= new NincsmailPage(getDriver());
        emailField.sendKeys(nincsmailPage.getEmail());
        continueButton.click();
        B_CompleteCreateAccountPage completeCreateAccount = new B_CompleteCreateAccountPage(getDriver());
        completeCreateAccount.completeSignUp();
    }
}
