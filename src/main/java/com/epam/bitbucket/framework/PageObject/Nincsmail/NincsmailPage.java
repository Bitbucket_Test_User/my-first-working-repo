package com.epam.bitbucket.framework.PageObject.Nincsmail;

import com.epam.bitbucket.framework.Parent;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import static com.epam.bitbucket.utility.Utility.generateName;
import static com.epam.bitbucket.utility.Utility.readFromPropertiesFile;

/**
 * Created by Anita_Horvath on 17/02/2017.
 */
public class NincsmailPage extends Parent{

    @FindBy(id="GenEmail")
    private WebElement emailField;

    @FindBy(id="btnValidEmailCim")
    private WebElement submitButton;

    private String email = "";
    private NincsmailSubPage nincsmailSubPage = null;

    public NincsmailPage(WebDriver driver){
        super(driver);
    }

    public void loadSite(){
        getDriver().get(readFromPropertiesFile("MAIL_URL"));
    }

    public String getEmail(){
        return email;
    }

    public void createUser(){
        emailField.sendKeys(generateName("test_user"));
        submitButton.click();
        nincsmailSubPage = new NincsmailSubPage(getDriver());
        email = nincsmailSubPage.getEmail();
        // System.out.println("The new email address is: " + email);
    }

    public void deleteUser() {
        nincsmailSubPage = new NincsmailSubPage(getDriver());
        nincsmailSubPage.deleteUser();
    }

}
