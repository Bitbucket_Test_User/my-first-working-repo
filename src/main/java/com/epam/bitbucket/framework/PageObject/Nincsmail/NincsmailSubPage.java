package com.epam.bitbucket.framework.PageObject.Nincsmail;

import com.epam.bitbucket.framework.Parent;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import static com.epam.bitbucket.utility.Utility.waitUntilElementToBeClickable;

/**
 * Created by Anita_Horvath on 20/02/2017.
 */
public class NincsmailSubPage extends Parent{

    @FindBy(className = "emailAdress")
    private WebElement email;

    @FindBy(className = "deleteMailbox")
    private WebElement deleteButton;

    @FindBy(className = "DeleteMailboxConfirmYes")
    private WebElement deleteConfirmButton;

    public NincsmailSubPage(WebDriver driver){
        super(driver);
    }

    public String getEmail(){
        waitUntilElementToBeClickable(email, getDriver());
        return email.getText();
    }

    public void deleteUser() { //Ádám: Miért nem tud ráklikkelni ha megtalálja és látja az elemet? --------------------------------
        deleteButton.click();
        /*WebDriverWait wait = new WebDriverWait(driver, 10);
        wait.until(ExpectedConditions.elementToBeClickable(deleteConfirmButton));*/
        deleteConfirmButton.click();
        /*deleteConfirmButton.getText();  // Chrome-ban első kattintásra sikerül, FF-ban még ezzel a megoldással se mindig-----------
        deleteConfirmButton.getAttribute("href");
        deleteConfirmButton.click();*/

    }
}
