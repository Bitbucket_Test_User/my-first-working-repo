package com.epam.bitbucket.framework.PageObject.Repositories;

import com.epam.bitbucket.framework.Parent;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

/**
 * Created by Anita_Horvath on 20/02/2017.
 */
public class B_CreateRepositoriesPage extends Parent {

    @FindBy(id = "id_name")
    private WebElement repoName;

    @FindBy(id = "id_is_private")
    private WebElement repoIsPrivate;

    @FindBy(css = "[type=\"submit\"]")
    private WebElement createButton;

    public B_CreateRepositoriesPage(WebDriver driver){
        super(driver);
    }

    public void fillRepoNameField(String name){
        repoName.sendKeys(name);
    }

    public void fillPrivateRepo(boolean privateRepo){
        if (!privateRepo)
            repoIsPrivate.click();
    }

    public void pressCreateButton(){
        createButton.click();
    }

    public void createRepo(String name, boolean privateRepo){
        repoName.sendKeys(name);
        if (!privateRepo)
            repoIsPrivate.click();
        createButton.click();
    }
}
