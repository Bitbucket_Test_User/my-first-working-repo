package com.epam.bitbucket.framework.PageObject.Repositories;

import com.epam.bitbucket.framework.Parent;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import static com.epam.bitbucket.utility.Utility.waitUntilElementToBeClickable;

/**
 * Created by Anita_Horvath on 20/02/2017.
 */
public class B_RepositoriesPage extends Parent{

    @FindBy(id = "create-repo")
    private WebElement createButton;

    public B_RepositoriesPage(WebDriver driver){
        super(driver);
    }

    public void createRepo(String name, boolean privateRepo){
        waitUntilElementToBeClickable(createButton,getDriver());
        createButton.click();
        B_CreateRepositoriesPage createRepositoriesPage = new B_CreateRepositoriesPage(getDriver());
        createRepositoriesPage.createRepo(name, privateRepo);
    }
}
