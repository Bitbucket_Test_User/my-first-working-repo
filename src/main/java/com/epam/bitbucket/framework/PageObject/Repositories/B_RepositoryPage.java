package com.epam.bitbucket.framework.PageObject.Repositories;

import com.epam.bitbucket.framework.Parent;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import static com.epam.bitbucket.utility.Utility.waitUntilElementToBeClickable;

/**
 * Created by Anita_Horvath on 21/02/2017.
 */
public class B_RepositoryPage extends Parent{

    @FindBy(id = "repo-settings-link")
    private WebElement settings;

    public B_RepositoryPage(WebDriver driver){
        super(driver);
    }

    public void pressSettings(){
        waitUntilElementToBeClickable(settings,getDriver());
        settings.click();
    }

    public boolean validateRepoSettings(boolean privateRepo){
        waitUntilElementToBeClickable(settings,getDriver());
        settings.click();
        B_RepositorySettingsPage repoSettings = new B_RepositorySettingsPage(getDriver());
        return repoSettings.validateRepoSettings(privateRepo);
    }

    public void deleteRepo(){
        waitUntilElementToBeClickable(settings,getDriver());
        settings.click();
        B_RepositorySettingsPage repoSettings = new B_RepositorySettingsPage(getDriver());
        repoSettings.deleteRepo();
    }
}
