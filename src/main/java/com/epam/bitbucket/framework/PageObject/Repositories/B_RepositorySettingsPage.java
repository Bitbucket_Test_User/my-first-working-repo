package com.epam.bitbucket.framework.PageObject.Repositories;

import com.epam.bitbucket.framework.Parent;
import org.openqa.selenium.*;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;

import static com.epam.bitbucket.utility.Utility.waitUntilElementToBeClickable;

/**
 * Created by Anita_Horvath on 21/02/2017.
 */
public class B_RepositorySettingsPage extends Parent{

    @FindBy(id = "id_is_private")
    private WebElement repoIsPrivate;

    @FindBy(id = "delete-repo-btn")
    private WebElement deleteRepoButton;

    @FindBy(css = "button[class$=\"dialog-submit\"]")
    private WebElement confirmDeleteRepoButton;

    public B_RepositorySettingsPage(WebDriver driver){
        super(driver);
    }

    public boolean validateRepoSettings(boolean privateRepo){
        if (repoIsPrivate.isSelected() == privateRepo)
            return true;
            else
            return false;
    }

    public void deleteRepo(){
        waitUntilElementToBeClickable(deleteRepoButton,getDriver());
        deleteRepoButton.click();

        Actions action = new Actions(getDriver());
        action.sendKeys(Keys.F12)
                .perform();

        try {
            Thread.sleep(5000);  // the worst case of explicit waits
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        /*WebDriverWait wait = new WebDriverWait(driver, 10);
        wait = new WebDriverWait(driver, 10);
        wait.until(ExpectedConditions.visibilityOf(driver.findElement(By.id("delete-repo-dialog"))));*/

        getDriver().findElement(By.cssSelector("div[id=\"delete-repo-dialog\"][aria-hidden=\"false\"]"));
        getDriver().findElement(By.className("aui-dialog2-footer"));
        getDriver().findElement(By.className("aui-dialog2-footer-actions")); // Able to locate element
        getDriver().findElement(By.linkText("Delete"));  // Unable to locate element: {"method":"link text","selector":"Delete"} ------------------------------------------------------------------

        //waitUntilElementToBeClickable(confirmDeleteRepoButton,getDriver()); // Ádám: ott van, a firebug is kimutatja, és mégsem látja -----------------------------------------------------------
        confirmDeleteRepoButton.click();
    }

    /*public boolean waitForJSandJQueryToLoad() {

        WebDriverWait wait = new WebDriverWait(getDriver(), 30);

        // wait for jQuery to load
        ExpectedCondition<Boolean> jQueryLoad = new ExpectedCondition<Boolean>() {
            public Boolean apply(WebDriver driver) {
                try {
                    return ((Long)((JavascriptExecutor)driver).executeScript("return jQuery.active") == 0);
                }
                catch (Exception e) {
                    // no jQuery present
                    return true;
                }
            }
        };

        // wait for Javascript to load
        ExpectedCondition<Boolean> jsLoad = new ExpectedCondition<Boolean>() {
            public Boolean apply(WebDriver driver) {
                return ((JavascriptExecutor)driver).executeScript("return document.readyState")
                        .toString().equals("complete");
            }
        };

        return wait.until(jQueryLoad) && wait.until(jsLoad);
    }*/

}
