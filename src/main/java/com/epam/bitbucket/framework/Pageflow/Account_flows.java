package com.epam.bitbucket.framework.Pageflow;

import com.epam.bitbucket.framework.PageObject.B_DashboardPage;
import com.epam.bitbucket.framework.PageObject.B_HomePage;
import com.epam.bitbucket.framework.PageObject.B_LogInPage;
import com.epam.bitbucket.framework.PageObject.CreateAccount.B_CompleteCreateAccountPage;
import com.epam.bitbucket.framework.PageObject.CreateAccount.B_CreateAccountPage;
import com.epam.bitbucket.framework.PageObject.Nincsmail.NincsmailPage;
import org.openqa.selenium.WebDriver;

import static com.epam.bitbucket.utility.Utility.readFromPropertiesFile;

/**
 * Created by Anita_Horvath on 27/02/2017.
 */
public class Account_flows {

    /**
     * Clicks on the user dropdown icon and
     * returns the user's name
     *
     * @param driver
     * @return the name of the user logged in
     */
    public String getLoggedUserName(WebDriver driver){
        B_DashboardPage dashboardPage = new B_DashboardPage(driver);
        dashboardPage.pressUserDropdown();
        String name = dashboardPage.getProfileName();
        dashboardPage.pressLogo();
        return name;
    }

    /**
     * Clicks on the user dropdown icon and
     * returns the user's email
     *
     * @param driver
     * @return the email of the user logged in
     */
    public String getLoggedUserEmail(WebDriver driver){
        B_DashboardPage dashboardPage = new B_DashboardPage(driver);
        dashboardPage.pressUserDropdown();
        String email = dashboardPage.getProfileEmail();
        dashboardPage.pressLogo();
        return email;
    }

    /**
     * Loads the website, presses the login button and
     * logs in on the login page
     *
     * @param driver
     */
    public void logIn(WebDriver driver){
        B_HomePage homePage = new B_HomePage(driver);
        homePage.loadSite();
        homePage.pressLogInButton();
        B_LogInPage logIn = new B_LogInPage(driver);
        logIn.fillEmailField(readFromPropertiesFile("EMAIL"));
        logIn.fillPasswordField(readFromPropertiesFile("PASSWORD"));
        logIn.pressLogInButton();
    }

    /**
     * Clicks on the user dropdown icon and logs out
     *
     * @param driver
     */
    public void logOut(WebDriver driver){
        B_DashboardPage dashboardPage = new B_DashboardPage(driver);
        dashboardPage.pressUserDropdown();
        dashboardPage.pressLogOutButton();
    }

    public void signUp(WebDriver driver){
        // TODO
        B_HomePage homePage = new B_HomePage(driver);
        homePage.loadSite();
        homePage.pressLogInButton();
        B_LogInPage logIn = new B_LogInPage(driver);
        logIn.pressSignUpButton();
        B_CreateAccountPage createAccount = new B_CreateAccountPage(driver);
        NincsmailPage nincsmailPage= new NincsmailPage(driver);
        createAccount.fillEmailField(nincsmailPage.getEmail());
        createAccount.pressContinueButton();
        B_CompleteCreateAccountPage completeCreateAccount = new B_CompleteCreateAccountPage(driver);
        completeCreateAccount.fillNameField("Auto Test User");
        completeCreateAccount.fillPasswordField("Autotest123");
        completeCreateAccount.checkCaptcha();
        completeCreateAccount.pressContinueButton();
        // TODO check your inbox and set username at almost done
    }

}
