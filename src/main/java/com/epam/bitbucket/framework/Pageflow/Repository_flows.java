package com.epam.bitbucket.framework.Pageflow;

import com.epam.bitbucket.framework.PageObject.B_DashboardPage;
import com.epam.bitbucket.framework.PageObject.Repositories.B_CreateRepositoriesPage;
import com.epam.bitbucket.framework.PageObject.Repositories.B_RepositoryPage;
import com.epam.bitbucket.framework.PageObject.Repositories.B_RepositorySettingsPage;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

/**
 * Created by Anita_Horvath on 28/02/2017.
 */
public class Repository_flows {

    /**
     * Clicks on the Repositories dropdown and
     * presses the Create Reporitory button
     * Creates a repository with the given name and private setting
     *
     * @param name - name of the repository
     * @param isPrivate - private setting of the reporistory
     * @param driver
     */
    public void createRepo(String name, boolean isPrivate, WebDriver driver){
        B_DashboardPage dashboardPage = new B_DashboardPage(driver);
        dashboardPage.pressRepoDropdown();
        dashboardPage.pressCreateRepoLink();
        B_CreateRepositoriesPage createRepositoriesPage = new B_CreateRepositoriesPage(driver);
        createRepositoriesPage.fillRepoNameField(name);
        createRepositoriesPage.fillPrivateRepo(isPrivate);
        createRepositoriesPage.pressCreateButton();
        dashboardPage.pressLogo();
    }

    /**
     * Opens the repositories page
     * Search after the given repository and
     * validates the repository settings
     *
     * @param name - name of the repository
     * @param isPrivate - private setting of the reporistory
     * @param driver
     * @return a boolean - is the repository created with the given data
     */
    public boolean repoIsCreated(String name, boolean isPrivate, WebDriver driver){
        boolean repoIsCreated = false;
        B_DashboardPage dashboardPage = new B_DashboardPage(driver);
        dashboardPage.pressRepoButton();
        for (WebElement e : dashboardPage.getRepoList()) {
            //System.out.println(e.getText());
            if (e.getText().equals(name)) {
                e.click();
                B_RepositoryPage repoPage = new B_RepositoryPage(driver);
                repoPage.pressSettings();
                B_RepositorySettingsPage repoSettings = new B_RepositorySettingsPage(driver);
                repoIsCreated = repoSettings.validateRepoSettings(isPrivate);
                break;
            }
        }
        dashboardPage.pressLogo();
        return repoIsCreated;
    }
}
