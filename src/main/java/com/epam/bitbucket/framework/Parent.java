package com.epam.bitbucket.framework;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.PageFactory;

/**
 * Created by Anita_Horvath on 24/02/2017.
 */
public class Parent {
    private WebDriver driver = null;

    public Parent(WebDriver driver){
        this.driver = driver;
        PageFactory.initElements(driver, this);
    }

    public WebDriver getDriver(){
        return driver;
    }
}
