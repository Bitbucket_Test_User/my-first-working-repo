package com.epam.bitbucket.utility;

import io.github.bonigarcia.wdm.ChromeDriverManager;
import io.github.bonigarcia.wdm.FirefoxDriverManager;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

/**
 * Created by Anita_Horvath on 02/03/2017.
 */
public class BrowserFactory {
    public static WebDriver setBrowser(String browser){
        WebDriver driver = null;

        /*if (browser.equalsIgnoreCase("firefox")) {
            //System.setProperty("webdriver.gecko.driver",readFromPropertiesFile("GECKO_DRIVER"));
            driver = new FirefoxDriver();

            // tesztnek
            //FirefoxProfile profile = new FirefoxProfile();
            //try {
            //    profile.addExtension(new File("C:\\Users\\Anita_Matyas@epam.com\\Downloads\\firebug-2.0.18-fx.xpi"));
            //} catch (IOException e) {
            //    e.printStackTrace();
            //}
            //driver = new FirefoxDriver(profile);

            driver.manage().window().maximize();
        }
        else if (browser.equalsIgnoreCase("chrome")) {
            System.setProperty("webdriver.chrome.driver", readFromPropertiesFile("CHROME_DRIVER"));
            driver = new ChromeDriver();
            driver.manage().window().maximize();
        }*/

        if (browser.equalsIgnoreCase("firefox")) {
            FirefoxDriverManager.getInstance().setup();
            driver = new FirefoxDriver();

        } else if (browser.equalsIgnoreCase("chrome")) {
            ChromeDriverManager.getInstance().setup();
            driver = new ChromeDriver();
        }
        return driver;
    }
}
