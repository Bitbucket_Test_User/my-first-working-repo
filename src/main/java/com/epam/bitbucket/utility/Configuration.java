package com.epam.bitbucket.utility;

import com.epam.bitbucket.framework.PageObject.Nincsmail.NincsmailPage;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.interactions.Actions;
import org.testng.annotations.*;

import static com.epam.bitbucket.utility.Utility.readFromPropertiesFile;

/**
 * Created by Anita_Horvath on 17/02/2017.
 */
public class Configuration{
    private NincsmailPage nincsmailPage = null;
    private static WebDriver driver = null;

    @Parameters("browser")
    @BeforeTest
    public void setupBrowser(String browser){
        driver = BrowserFactory.setBrowser(browser);
        driver.manage().window().maximize();
    }

    //@BeforeTest (dependsOnMethods = "setupBrowser")
    public void createNincsmailUser(){
        nincsmailPage = new NincsmailPage(driver);
        nincsmailPage.loadSite();
        nincsmailPage.createUser();
    }

    /*@AfterMethod
    public void takeScreenshotOnFailure(ITestResult testResult){
        if (testResult.getStatus() == ITestResult.FAILURE) {
            File file = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
            try {
                FileHandler.copy(file, new File("D:\\Anita_Horvath\\Local_repository\\target\\tests\\" + testResult.getName() + "_screenshot.png"));
                // FileUtils.copyFile(file, new File("D:\\testScreenShot.jpg"));
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }*/

    //@AfterTest
    public void deleteNincsmailUser(){
        Actions action= new Actions(driver);
        action.keyDown(Keys.CONTROL)
              .keyDown(Keys.SHIFT)
              .sendKeys(Keys.TAB)
              .keyUp(Keys.SHIFT)
              .keyUp(Keys.CONTROL)
              .perform();
        //driver.navigate().refresh();  // Ádám: refresh nélkül h lehet belekattintani a tabba?-------------------------------------?
        driver.get(readFromPropertiesFile("MAIL_URL")); // A Chrome miatt mert ott csak egy tab van, és a refresh nem elég------------------------------------
        nincsmailPage.deleteUser();
    }

    @AfterTest //(dependsOnMethods = "deleteNincsmailUser")
    public void closeBrowser(){
        driver.quit();
    }

    public static WebDriver getDriver(){
        return driver;
    }
}
