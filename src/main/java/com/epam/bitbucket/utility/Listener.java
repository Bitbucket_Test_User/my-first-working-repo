package com.epam.bitbucket.utility;

import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.io.FileHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.testng.*;

import java.io.File;
import java.io.IOException;

import static com.epam.bitbucket.utility.Configuration.getDriver;

/**
 * Created by Anita_Horvath on 27/02/2017.
 */
public class Listener implements ITestListener, ISuiteListener, IInvokedMethodListener {

    private static Logger LOG = null;

    public void beforeInvocation(IInvokedMethod iInvokedMethod, ITestResult iTestResult) {
        LOG.info("Starting method: " + iInvokedMethod.toString());
    }

    public void afterInvocation(IInvokedMethod iInvokedMethod, ITestResult iTestResult) {
        LOG.info("Finishing method: " + iInvokedMethod.toString());
    }

    public void onStart(ISuite iSuite) {
        System.setProperty("logback.configurationFile", "\\src\\main\\resources\\logback.xml");
        LOG = LoggerFactory.getLogger(Listener.class);
        LOG.info("------------------------ Start testing ------------------------");
        LOG.info("Starting suite: " + iSuite.getName());
    }

    public void onFinish(ISuite iSuite) {
        LOG.info("Finishing suite: " + iSuite.getName());
        LOG.info("------------------------ Finish testing -----------------------");
    }

    public void onTestStart(ITestResult iTestResult) {
        LOG.info("Starting test: " + iTestResult.getName());
    }

    public void onTestSuccess(ITestResult iTestResult) {
        LOG.info("Test success: " + iTestResult.getName());
    }

    public void onTestFailure(ITestResult iTestResult) {
        LOG.info("Test failed: " + iTestResult.getName());
        File file = ((TakesScreenshot)getDriver()).getScreenshotAs(OutputType.FILE);
        try {
            FileHandler.copy(file, new File("D:\\Anita_Horvath\\Local_repository\\target\\tests\\" + iTestResult.getName() + "_screenshot.png"));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void printTestResults(ITestResult iTestResult){

    }

    public void onTestSkipped(ITestResult iTestResult) {
        LOG.info("Test skipped: " + iTestResult.getName());
    }

    public void onTestFailedButWithinSuccessPercentage(ITestResult iTestResult) {

    }

    public void onStart(ITestContext iTestContext) {

    }

    public void onFinish(ITestContext iTestContext) {

    }
}
