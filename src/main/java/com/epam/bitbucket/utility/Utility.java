package com.epam.bitbucket.utility;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Calendar;
import java.util.List;
import java.util.Properties;

/**
 * Created by Anita_Horvath on 23/02/2017.
 */
public class Utility {
    public static void openNewTab(WebDriver driver){ //Ádám: chrome-ban van-e erre megoldásuk, vagy mindent ugyanazon a tabon tesztelnek?---------------------------
        Actions action = new Actions(driver);
        action.keyDown(Keys.CONTROL)
                .sendKeys("t")
                .keyUp(Keys.CONTROL)
                .perform();
    }

    public static String generateName(String name){
        Calendar c = Calendar.getInstance();
        return name + c.getTimeInMillis() % 1000000;
    }

    public static void waitUntilVisibilityOfAllElements(List<WebElement> repoList, WebDriver driver){
        WebDriverWait wait = new WebDriverWait(driver, 10);
        wait.until(ExpectedConditions.visibilityOfAllElements(repoList));
    }

    public static void waitUntilElementToBeClickable(WebElement element, WebDriver driver){
        WebDriverWait wait = new WebDriverWait(driver, 10);
        wait.until(ExpectedConditions.elementToBeClickable(element));
    }

    public static String readFromPropertiesFile(String param){
        Properties prop = new Properties();
        InputStream input = null;
        try {
            input = new FileInputStream("src\\main\\resources\\config.properties");

            // load a properties file
            prop.load(input);

            // get the property value
            return prop.getProperty(param);

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (input != null)
                try {
                    input.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
        }

        return "";
    }

}
