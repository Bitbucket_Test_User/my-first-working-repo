package com.epam.bitbucket.test;

import com.epam.bitbucket.framework.PageObject.B_DashboardPage;
import com.epam.bitbucket.framework.Pageflow.Account_flows;
import com.epam.bitbucket.framework.Pageflow.Repository_flows;
import org.testng.annotations.Test;

import static com.epam.bitbucket.utility.Configuration.getDriver;
import static com.epam.bitbucket.utility.Utility.generateName;
import static com.epam.bitbucket.utility.Utility.openNewTab;
import static com.epam.bitbucket.utility.Utility.readFromPropertiesFile;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.core.Is.is;

/**
 * Created by Anita_Horvath on 17/02/2017.
 */

//@Listeners(com.epam.bitbucket.utility.Listener.class)

public class TestCases {
    private String repoName = "";
    private Account_flows account_flows = new Account_flows();
    private Repository_flows repo_flows = new Repository_flows();
    private B_DashboardPage dashboardPage = null;


    //@Test
    public void validateSignUp() {
        // sign in
        // TODO
        openNewTab(getDriver());
        account_flows.signUp(getDriver());
    }

    @Test //(dependsOnMethods = "validateSignUp")
    public void validateLogIn() {
        // login
        account_flows.logIn(getDriver());

        // validate login
        assertThat("Validate username",account_flows.getLoggedUserName(getDriver()), is(readFromPropertiesFile("NAME")));
        assertThat("Validate user email",account_flows.getLoggedUserEmail(getDriver()), is(readFromPropertiesFile("EMAIL")));
    }

    @Test (dependsOnMethods = "validateLogIn")
    public void validateCreateRepo() {
        // create repo
        repoName = generateName("MyRepo");
        repo_flows.createRepo(repoName, false, getDriver());


        // validate create repo
        dashboardPage = new B_DashboardPage(getDriver());
        //dashboardPage.validateCreateRepo(repoName, false);
        assertThat("Repository with this name and private setting is created",repo_flows.repoIsCreated(repoName, false, getDriver()), is(true));
    }

    //@Test (dependsOnMethods = "validateCreateRepo")
    public void deleteRepo(){
        // TODO
        dashboardPage.deleteRepo(repoName);
    }

    @Test (dependsOnMethods = "validateCreateRepo") //(dependsOnMethods = "deleteRepo")
    public void logOut(){
        account_flows.logOut(getDriver());
    }
}
